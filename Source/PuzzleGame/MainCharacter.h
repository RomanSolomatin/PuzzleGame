// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

UCLASS()
class PUZZLEGAME_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

	//Base movement rates

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Movement functions
	void MoveForward(float Value);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	bool bCanMoveForward = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	bool bWallForward = false;

	void MoveBackward(float Value);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	bool bCanMoveBackward = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	//Set to true so player can't go out of the map at start (bottom left)position
	bool bWallBackward = true;

	void MoveRight(float Value);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	bool bCanMoveRight = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	bool bWallRight = false;

	void MoveLeft(float Value);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	bool bCanMoveLeft = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	//Set to true so player can't go out of the map at start (bottom left)position
	bool bWallLeft = true;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
