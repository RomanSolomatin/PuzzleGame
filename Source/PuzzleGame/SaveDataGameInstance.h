// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "SaveDataGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEGAME_API USaveDataGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Money)
	float BlueMoney;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Money)
	float RedMoney;

public:

	void AddBlue();

	void AddRed();

	FString GetCurrentMapName();
};
