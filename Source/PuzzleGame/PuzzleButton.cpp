// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleGame.h"
#include "PuzzleButton.h"
#include "MainCharacter.h"
#include "SaveDataGameInstance.h"
#include "PuzzleGameMode.h"

// Sets default values
APuzzleButton::APuzzleButton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USaveData* LoadGameInstanceRed = Cast<USaveData>(UGameplayStatics::CreateSaveGameObject(USaveData::StaticClass()));
	LoadGameInstanceRed = Cast<USaveData>(UGameplayStatics::LoadGameFromSlot(LoadGameInstanceRed->RedSlotName, LoadGameInstanceRed->UserIndex));

	//Init overlap sound
	static ConstructorHelpers::FObjectFinder<USoundBase>Sound(TEXT("SoundWave'/Game/Sounds/OverlapSound.OverlapSound'"));
	if (Sound.Succeeded())
	{
		OverlapSound = Sound.Object;
	}

	// Init cube
	static ConstructorHelpers::FObjectFinder<UStaticMesh>DemoCube(TEXT("StaticMesh'/Game/Models/Buttons/button1.button1'"));
	// If we found that cube
	if (DemoCube.Succeeded())
	{
		CubeComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cube"));
		CubeComponent->SetStaticMesh(DemoCube.Object);
		CubeComponent->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
		CubeComponent->SetRelativeScale3D(FVector(1.f, 1.f, 1.f));
	}

	//Init collision for this cube
	Collision = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	Collision->SetRelativeLocation(FVector(0.f, 0.f, 150.f));
	Collision->SetRelativeScale3D(FVector(1.5f, 1.5f, 1.f));
	Collision->SetupAttachment(CubeComponent);
	Collision->OnComponentBeginOverlap.AddDynamic(this, &APuzzleButton::Overlap);

	//Init materials for this cube
	static ConstructorHelpers::FObjectFinder<UMaterial>IsOnMaterial(TEXT("Material'/Game/Textures/Blue_Mat.Blue_Mat'"));
	// If we found that material
	if (IsOnMaterial.Succeeded())
	{
		IsOn = CreateDefaultSubobject<UMaterial>(TEXT("IsOnMaterial"));
		IsOn = (UMaterial*)IsOnMaterial.Object;
		UMaterialInstanceDynamic* IsOnInstance = UMaterialInstanceDynamic::Create(IsOn, CubeComponent);

	}

	static ConstructorHelpers::FObjectFinder<UMaterial>IsOffMaterial(TEXT("Material'/Game/Textures/Red_Mat.Red_Mat'"));
	// If we found that material
	if (IsOffMaterial.Succeeded())
	{
		IsOff = CreateDefaultSubobject<UMaterial>(TEXT("IsOffMaterial"));
		IsOff = (UMaterial*)IsOffMaterial.Object;
		UMaterialInstanceDynamic* IsOffInstance = UMaterialInstanceDynamic::Create(IsOff, CubeComponent);
	}
	static ConstructorHelpers::FObjectFinder<UMaterial>SecondMat(TEXT("Material'/Game/Pics/Floor_Mat.Floor_Mat'"));
	// If we found that material
	if (SecondMat.Succeeded())
	{
		SecondMaterial = CreateDefaultSubobject<UMaterial>(TEXT("SecondfMaterial"));
		SecondMaterial = (UMaterial*)SecondMat.Object;
		UMaterialInstanceDynamic* SecondMaterialInstance = UMaterialInstanceDynamic::Create(SecondMaterial, CubeComponent);
		CubeComponent->SetMaterial(0, SecondMaterial);
	}
	
	//Init partical system
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleName(TEXT("ParticleSystem'/Game/Textures/Button_Particle.Button_Particle'"));
	OverlapParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ArbitraryParticleName"));
	//If we found it
	if (ParticleName.Succeeded()) {
		OverlapParticle->SetTemplate(ParticleName.Object);
	}
	OverlapParticle->bAutoActivate = false;
	OverlapParticle->SetHiddenInGame(false);
	//Attach it to the cube
	OverlapParticle->AttachTo(Cast<USceneComponent>(FindComponentByClass(UStaticMeshComponent::StaticClass())));

}

// Called when the game starts or when spawned
void APuzzleButton::BeginPlay()
{
	Super::BeginPlay();
	//Set random puzzle to solve
	bIsOn = FMath::RandBool();

	//Set particle location and rotation at every level
	//No idea why scale set is here and not in constructor, but that's the only way it works.
	if (GetCurrentMapName().Contains(TEXT("Level1")))
	{
		OverlapParticle->SetRelativeLocation(FVector(0.f, 0.f, 30.f));
		OverlapParticle->SetRelativeScale3D(FVector(2.f, 2.f, 2.f));
	}

	else if (GetCurrentMapName().Contains(TEXT("Level2")))
	{
		OverlapParticle->SetRelativeLocation(FVector(0.f, 0.f, 20.f));
		OverlapParticle->SetRelativeScale3D(FVector(2.f, 2.f, 2.f));
	}

	else if (GetCurrentMapName().Contains(TEXT("Level3")))
	{
		OverlapParticle->SetRelativeLocation(FVector(0.f, 0.f, 10.f));
		OverlapParticle->SetRelativeScale3D(FVector(2.f, 2.f, 2.f));
	}

	else if (GetCurrentMapName().Contains(TEXT("Zen")))
	{
		OverlapParticle->SetRelativeLocation(FVector(0.f, 0.f, 10.f));
		OverlapParticle->SetRelativeScale3D(FVector(2.f, 2.f, 2.f));
	}

}

// Called every frame
void APuzzleButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	//Set button's materials
	if (bIsOn)
	{
		CubeComponent->SetMaterial(1, IsOn);
	}
	else
	{
		CubeComponent->SetMaterial(1, IsOff);
	}

}

UParticleSystemComponent* APuzzleButton::GetParticle()
{
	return OverlapParticle;
}

FString APuzzleButton::GetCurrentMapName()
{
	return GetWorld()->GetMapName();
}

void APuzzleButton::Overlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if ((OtherActor->GetClass()) == (AMainCharacter::StaticClass()))
	{
			//play sound and effect
			UGameplayStatics::PlaySound2D(GetWorld(), OverlapSound, 0.3f);
			OverlapParticle->ActivateSystem();

		if (bIsOn)
		{
			bIsOn = false;
		}
		else
		{
			bIsOn = true;
		}
			// Clean all variables in loop
			TempArray.Empty();
			Itr = 0;
			// For all actors of class
			for (TActorIterator<APuzzleButton> ActorItr(GetWorld()); ActorItr; ++ActorItr)
			{
				//Get every button's bIsOn and insert it to array
				TempArray.Insert(ActorItr->bIsOn, Itr);
				++Itr;
			}
			//If all buttons set to true
			if (!(TempArray.Contains(false)))
			{
				GLog->Log("All true");
				
				//Create reference to game instance, where we store temp money values
				USaveDataGameInstance* SaveMoneyInstance = Cast<USaveDataGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
				//Count how many money player won for this game
				SaveMoneyInstance->AddBlue();
				//Create save game reference, where we store permanent money values
				USaveData* LoadGameInstanceBlue = Cast<USaveData>(UGameplayStatics::CreateSaveGameObject(USaveData::StaticClass()));
				//Load money value from save slot
				LoadGameInstanceBlue = Cast<USaveData>(UGameplayStatics::LoadGameFromSlot(LoadGameInstanceBlue->BlueSlotName, LoadGameInstanceBlue->UserIndex));
				//Add temp value to permanent
				LoadGameInstanceBlue->BlueMoney += SaveMoneyInstance->BlueMoney;
				//And save it
				UGameplayStatics::SaveGameToSlot(LoadGameInstanceBlue, LoadGameInstanceBlue->BlueSlotName, LoadGameInstanceBlue->UserIndex);

				APuzzleGameMode* GameMode = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
				if (GameMode)
				{
					GameMode->GameWon();
					GameMode->bIsAllBlue = true;
				}
			}

			//If all buttons set to false
			if (!(TempArray.Contains(true)))
			{
				GLog->Log("All false");
				
				//Create reference to game instance, where we store temp money values
				USaveDataGameInstance* SaveMoneyInstance = Cast<USaveDataGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
				//Count how many money player won for this game
				SaveMoneyInstance->AddRed();
				//Create save game reference, where we store permanent money values
				USaveData* LoadGameInstanceRed = Cast<USaveData>(UGameplayStatics::CreateSaveGameObject(USaveData::StaticClass()));
				//Load money value from save slot
				LoadGameInstanceRed = Cast<USaveData>(UGameplayStatics::LoadGameFromSlot(LoadGameInstanceRed->RedSlotName, LoadGameInstanceRed->UserIndex));
				//Add temp value to permanent
				LoadGameInstanceRed->RedMoney += SaveMoneyInstance->RedMoney;
				//And save it
				UGameplayStatics::SaveGameToSlot(LoadGameInstanceRed, LoadGameInstanceRed->RedSlotName, LoadGameInstanceRed->UserIndex);

				APuzzleGameMode* GameMode = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
				if (GameMode)
				{
					GameMode->GameWon();
					GameMode->bIsAllBlue = false;
				}
			}
	}
}