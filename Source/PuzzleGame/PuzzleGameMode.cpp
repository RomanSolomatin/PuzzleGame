// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleGame.h"
#include "PuzzleGameMode.h"
#include "MainCharacter.h"
#include "PuzzleButton.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

APuzzleGameMode::APuzzleGameMode()
{
	//Init Time's Up sound
	static ConstructorHelpers::FObjectFinder<USoundBase>Sound(TEXT("SoundWave'/Game/Sounds/TimesUp.TimesUp'"));
	if (Sound.Succeeded())
	{
	TimesUpSound = Sound.Object;
	}
}

void APuzzleGameMode::BeginPlay()
{
	Super::BeginPlay();

	//Load our save data

	//Create reference to red save
	USaveData* LoadGameInstanceRed = Cast<USaveData>(UGameplayStatics::CreateSaveGameObject(USaveData::StaticClass()));
	//Check if save data exists
	bool bIsRedExists = UGameplayStatics::DoesSaveGameExist("RedSaveSlot", 0);
	//If it is, load it
	if (bIsRedExists)
		LoadGameInstanceRed = Cast<USaveData>(UGameplayStatics::LoadGameFromSlot(LoadGameInstanceRed->RedSlotName, LoadGameInstanceRed->UserIndex));
	else
		//If it's not, create save file by saving defaults
		UGameplayStatics::SaveGameToSlot(LoadGameInstanceRed, LoadGameInstanceRed->RedSlotName, LoadGameInstanceRed->UserIndex);

	//Create reference to blue save
	USaveData* LoadGameInstanceBlue = Cast<USaveData>(UGameplayStatics::CreateSaveGameObject(USaveData::StaticClass()));
	//Check if save data exists
	bool bIsBlueExists = UGameplayStatics::DoesSaveGameExist("BlueSaveSlot", 0);
	//If it is, load it
	if (bIsBlueExists)
		//If it's not, create save file by saving defaults
		LoadGameInstanceBlue = Cast<USaveData>(UGameplayStatics::LoadGameFromSlot(LoadGameInstanceBlue->BlueSlotName, LoadGameInstanceBlue->UserIndex));
	else
		UGameplayStatics::SaveGameToSlot(LoadGameInstanceRed, LoadGameInstanceRed->BlueSlotName, LoadGameInstanceRed->UserIndex);
	

	//If we set GameplayHUDClass
	if(GameplayHUDClass)
	GameplayWidget = CreateWidget<UUserWidget>(GetWorld(), GameplayHUDClass);

	//If we have created widget
	if(GameplayWidget)
	GameplayWidget->AddToViewport();

	//Get player controller to work with
	APlayerController* PC = Cast<APlayerController>(UGameplayStatics::GetPlayerController(this, 0));

	//Settings for mouse cursor (for debugging purposes only)
	if (PC)
	{
		PC->bShowMouseCursor = true;
		PC->bEnableClickEvents = true;
	}

}

void APuzzleGameMode::GameWon()
{
	//Clean the viewport
	GameplayWidget->RemoveFromViewport();
	//Activate particle effect in each one button
	for (TActorIterator<APuzzleButton> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ActorItr->GetParticle()->ActivateSystem();
	}
	//Disable player movement
	APlayerController* PC = Cast<APlayerController>(UGameplayStatics::GetPlayerController(this, 0));
	DisableInput(PC);
	//Call game won HUD
	GetWorldTimerManager().SetTimer(TimerHandler, this, &APuzzleGameMode::CallWonMenu, 1.f, true);

}

void APuzzleGameMode::GameLose()
{
	//Clean the viewport and disable input
	GameplayWidget->RemoveFromViewport();
	APlayerController* PC = Cast<APlayerController>(UGameplayStatics::GetPlayerController(this, 0));
	DisableInput(PC);
	//Play the time's up sound
	UGameplayStatics::PlaySound2D(GetWorld(), TimesUpSound,0.3f,1.0f,2.0f);
	//Call game lose HUD
	GetWorldTimerManager().SetTimer(TimerHandler, this, &APuzzleGameMode::CallLoseMenu, 1.f, true);
}

void APuzzleGameMode::CallWonMenu()
{
	//If we set PlayerWonHUDClass
	if (PlayerWonHUDClass)
	{
	PlayertWonWidget = CreateWidget<UWonWidget>(GetWorld(), PlayerWonHUDClass);
	}

	//If we have created widget
	if (PlayertWonWidget)
	{
	PlayertWonWidget->AddToViewport();
	}
}


void APuzzleGameMode::CallLoseMenu()
{
	//If we set PlayerLoseHUDClass
	if (PlayerLoseHUDClass)
	{
		PlayerLoseWidget = CreateWidget<UWonWidget>(GetWorld(), PlayerLoseHUDClass);
	}

	//If we have created widget
	if (PlayerLoseWidget)
	{
		PlayerLoseWidget->AddToViewport();
	}
}
