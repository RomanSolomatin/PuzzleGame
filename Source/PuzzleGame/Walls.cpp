// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleGame.h"
#include "MainCharacter.h"
#include "Walls.h"


// Sets default values
AWalls::AWalls()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	// Init collision boxes for walls, and bind overlap events
	LeftWall = CreateDefaultSubobject<UBoxComponent>(TEXT("Left Wall"));
	LeftWall->OnComponentBeginOverlap.AddDynamic(this, &AWalls::LeftOverlapBegin);
	LeftWall->OnComponentEndOverlap.AddDynamic(this, &AWalls::LeftOverlapEnd);
	LeftWall->SetupAttachment(RootComponent);

	RightWall = CreateDefaultSubobject<UBoxComponent>(TEXT("Right Wall"));
	RightWall->SetupAttachment(RootComponent);
	RightWall->OnComponentBeginOverlap.AddDynamic(this, &AWalls::RightOverlapBegin);
	RightWall->OnComponentEndOverlap.AddDynamic(this, &AWalls::RightOverlapEnd);

	FwdWall = CreateDefaultSubobject<UBoxComponent>(TEXT("Forward Wall"));
	FwdWall->SetupAttachment(RootComponent);
	FwdWall->OnComponentBeginOverlap.AddDynamic(this, &AWalls::FwdOverlapBegin);
	FwdWall->OnComponentEndOverlap.AddDynamic(this, &AWalls::FwdOverlapEnd);

	Bcwdwall = CreateDefaultSubobject<UBoxComponent>(TEXT("Backward Wall"));
	Bcwdwall->SetupAttachment(RootComponent);
	Bcwdwall->OnComponentBeginOverlap.AddDynamic(this, &AWalls::BcwdOverlapBegin);
	Bcwdwall->OnComponentEndOverlap.AddDynamic(this, &AWalls::BcwdOverlapEnd);

}

// Called when the game starts or when spawned
void AWalls::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AWalls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//When we hit left wall
void AWalls::LeftOverlapBegin(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AMainCharacter* Actor = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Actor)
		//Player now can't go left
		Actor->bWallLeft = true;
}

//When we get out of left wall
void AWalls::LeftOverlapEnd(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AMainCharacter* Actor = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Actor)
		//Player now can go left
		Actor->bWallLeft = false;
}

//When we hit right wall
void AWalls::RightOverlapBegin(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AMainCharacter* Actor = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Actor)
		//Player now can't go right
		Actor->bWallRight = true;
}

//When we get out of right wall
void AWalls::RightOverlapEnd(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AMainCharacter* Actor = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Actor)
		//Player now can go right
		Actor->bWallRight = false;
}

//When we hit forward wall
void AWalls::FwdOverlapBegin(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AMainCharacter* Actor = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Actor)
		//Player now can't go forward
		Actor->bWallForward = true;
}

//When we get out of forward wall
void AWalls::FwdOverlapEnd(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AMainCharacter* Actor = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Actor)
		//Player now can't go forward
		Actor->bWallForward = false;
}

//When we hit back wall
void AWalls::BcwdOverlapBegin(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AMainCharacter* Actor = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Actor)
		//Player now can't go backward
		Actor->bWallBackward = true;
}

//When we get out of back wall
void AWalls::BcwdOverlapEnd(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AMainCharacter* Actor = Cast<AMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Actor)
		//Player now can't go backward
		Actor->bWallBackward = false;
}