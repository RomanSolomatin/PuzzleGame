// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "SaveData.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEGAME_API USaveData : public USaveGame
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Money)
	float BlueMoney = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Money)
	float RedMoney = 0;

public:


	UPROPERTY(VisibleAnywhere, Category = Basic)
	FString RedSlotName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FString BlueSlotName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	uint32 UserIndex;

	USaveData();
	
};
