// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleGame.h"
#include "TimerActor.h"
#include "PuzzleGameMode.h"
#include "PuzzleButton.h"


// Sets default values
ATimerActor::ATimerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

FString ATimerActor::GetCurrentMapName()
{
	return GetWorld()->GetMapName();
}

// Called when the game starts or when spawned
void ATimerActor::BeginPlay()
{
	Super::BeginPlay();

	if (GetCurrentMapName().Contains(TEXT("Level1")))
		Timer = 30;

	else if (GetCurrentMapName().Contains(TEXT("Level2")))
		Timer = 60;

	else if (GetCurrentMapName().Contains(TEXT("Level3")))
		Timer = 90;

	else if (GetCurrentMapName().Contains(TEXT("Zen")))
		Timer = 0;

	GetWorldTimerManager().SetTimer(TimerHandler, this, &ATimerActor::DecrementTimer, 1.f, true);

}

// Called every frame
void ATimerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void ATimerActor::DecrementTimer()
{
	--Timer;
	//GLog->Log("-1");
	if (Timer < 1)
	{
		GetWorldTimerManager().PauseTimer(TimerHandler);
		APuzzleGameMode* GameMode = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode)
			GameMode->GameLose();
	}
}

float ATimerActor::GetTimer()
{
	return Timer;
}