// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleGame.h"
#include "SaveDataGameInstance.h"

FString USaveDataGameInstance::GetCurrentMapName()
{
	return GetWorld()->GetMapName();
}


void USaveDataGameInstance::AddBlue()
{
	
	if(GetCurrentMapName().Contains(TEXT("Level1")))
	BlueMoney = 100;
	
	else if (GetCurrentMapName().Contains(TEXT("Level2")))
	BlueMoney = 300;

	else if (GetCurrentMapName().Contains(TEXT("Level3")))
	BlueMoney = 500;

	else if (GetCurrentMapName().Contains(TEXT("Zen")))
	BlueMoney = 50;
}

void USaveDataGameInstance::AddRed()
{

	if (GetCurrentMapName().Contains(TEXT("Level1")))
		RedMoney = 100;

	else if (GetCurrentMapName().Contains(TEXT("Level2")))
		RedMoney = 300;

	else if (GetCurrentMapName().Contains(TEXT("Level3")))
		RedMoney = 500;

	else if (GetCurrentMapName().Contains(TEXT("Zen")))
		RedMoney = 50;
}
