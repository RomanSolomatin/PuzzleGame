// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "WonWidget.h"
#include "PuzzleGameMode.generated.h"


/**
 * 
 */
UCLASS()
class PUZZLEGAME_API APuzzleGameMode : public AGameModeBase
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

public:

	APuzzleGameMode();

	//Time's up sound for game over state
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Sound, meta = (AllowPrivateAccess = "true"))
	class USoundBase* TimesUpSound;

	//Won state - all red or all blue
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsAllBlue;

	//Subclass for gameplay widget
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "State", Meta = (BlueprintProtected = "true"))
	TSubclassOf<class UUserWidget> GameplayHUDClass;

	//Pointer to active widget we'll add to viewport
	UPROPERTY()
	class UUserWidget* GameplayWidget;

	//Subclass for "You won" widget
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "State", Meta = (BlueprintProtected = "true"))
	TSubclassOf<class UWonWidget> PlayerWonHUDClass;

	//Pointer to active widget we'll add to viewport
	UPROPERTY()
	class UWonWidget* PlayertWonWidget;

	//Subclass for "You lose" widget
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "State", Meta = (BlueprintProtected = "true"))
	TSubclassOf<class UUserWidget> PlayerLoseHUDClass;

	//Pointer to active widget we'll add to viewport
	UPROPERTY()
	class UUserWidget* PlayerLoseWidget;

	FTimerHandle TimerHandler;

	//Functions to call when game was won or lose
	void GameWon();
	void GameLose();
	void CallWonMenu();
	void CallLoseMenu();

};
