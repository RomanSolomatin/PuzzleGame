// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TimerActor.generated.h"

UCLASS()
class PUZZLEGAME_API ATimerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATimerActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FTimerHandle TimerHandler;

	void DecrementTimer();

	UFUNCTION(BlueprintPure, Category = "Timer")
	float GetTimer();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Timer")
	float Timer = 10.f;

	UPROPERTY(EditAnywhere)
	TArray <bool> TempArray;

	// Custom Iterator for overlap array
	uint32 Itr = 0;

	FString GetCurrentMapName();
	
};
