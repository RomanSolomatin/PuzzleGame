// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "UnrealMathUtility.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "PuzzleButton.generated.h"

UCLASS()
class PUZZLEGAME_API APuzzleButton : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Materials, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* CubeComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collision, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* Collision;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Materials, meta = (AllowPrivateAccess = "true"))
	class UMaterial* IsOn;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Materials, meta = (AllowPrivateAccess = "true"))
	class UMaterial* IsOff;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Materials, meta = (AllowPrivateAccess = "true"))
	class UMaterial* SecondMaterial;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Sound, meta = (AllowPrivateAccess = "true"))
	class USoundBase* OverlapSound;
	UPROPERTY(VisibleAnywhere)
	class UParticleSystemComponent* OverlapParticle;
	
public:	
	// Sets default values for this actor's properties
	APuzzleButton();

	UFUNCTION()
	void Overlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	UParticleSystemComponent* GetParticle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Button state
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsOn;

	// Temporary array for button's state
	UPROPERTY(EditAnywhere)
	TArray <bool> TempArray;

	// Custom Iterator for overlap array
	uint32 Itr = 0;

	FString GetCurrentMapName();

};
