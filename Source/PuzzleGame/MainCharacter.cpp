// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleGame.h"
#include "SaveData.h"
#include "MainCharacter.h"
#include "PuzzleButton.h"


// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//CollisionCapsule setup
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.f);

	//Initialize turn rates
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	//Don't rotate camera when controller rotates:
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveBackward", this, &AMainCharacter::MoveBackward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveLeft", this, &AMainCharacter::MoveLeft);

}

void AMainCharacter::MoveForward(float Value)
{
	if (bCanMoveForward && !(bWallForward))
	{
		// Find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// Get forward vector and move in that direction
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		//SetActorLocationAndRotation(Direction * 30 + GetActorLocation(), YawRotation);
		AddMovementInput(Direction, 0.5);
		SetActorRotation(YawRotation);
	}
}

void AMainCharacter::MoveBackward(float Value)
{
	if (bCanMoveBackward && !(bWallBackward))
	{
		// Find out which way is backward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw -180, 0);

		// Get vector and move in that direction
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		//SetActorLocationAndRotation(Direction * 30 + GetActorLocation(), YawRotation);
		AddMovementInput(Direction, 0.5);
		SetActorRotation(YawRotation);
	}
}

void AMainCharacter::MoveRight(float Value)
{
	if (bCanMoveRight && !(bWallRight))
	{
		// Find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// Get vector and move in that direction
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		//SetActorLocationAndRotation(Direction * 30 + GetActorLocation(), YawRotation + FRotator(0, 90, 0));
		AddMovementInput(Direction, 0.5);
		SetActorRotation(YawRotation + FRotator(0, 90, 0));

	}
}

void AMainCharacter::MoveLeft(float Value)
{
	if (bCanMoveLeft && !(bWallLeft))
	{
		// Find out which way is left
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw - 180, 0);

		// Get vector and move in that direction
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		//SetActorLocationAndRotation(Direction * 30 + GetActorLocation(), YawRotation + FRotator(0, 90, 0));
		AddMovementInput(Direction, 0.5);
		SetActorRotation(YawRotation + FRotator(0, 90, 0));
	}
}

